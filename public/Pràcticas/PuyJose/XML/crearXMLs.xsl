<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:variable name="num" select="11"/>
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="Videojuegos">
        <Videojuego>
            <Nombre><xsl:value-of select="Videojuego[$num]/Nombre"/></Nombre>
            <xsl:element name="Genero">
                <xsl:attribute name="SubGen"><xsl:value-of select="Videojuego[$num]/Genero/@SubGen"/></xsl:attribute>
                <xsl:value-of select="Videojuego[$num]/Genero"/>
            </xsl:element>
            <InfoGeneral>
                <Foto><xsl:value-of select="Videojuego[$num]/InfoGeneral/Foto"/></Foto>
                <Enlace><xsl:value-of select="Videojuego[$num]/InfoGeneral/Enlace"/></Enlace>
                <Idiomas>
                    <xsl:for-each select="Videojuego[$num]/InfoGeneral/Idiomas/Idioma">
                        <Idioma><xsl:value-of select="."/></Idioma>
                    </xsl:for-each>
                </Idiomas>
                <xsl:element name="Desarrollador">
                    <xsl:attribute name="Logo"><xsl:value-of select="Videojuego[$num]/InfoGeneral/Desarrollador/@Logo"/></xsl:attribute>
                    <xsl:value-of select="Videojuego[$num]/InfoGeneral/Desarrollador"/>
                </xsl:element>
                <xsl:element name="Distribuidor">
                    <xsl:attribute name="Logo"><xsl:value-of select="Videojuego[$num]/InfoGeneral/Distribuidor/@Logo"/></xsl:attribute>
                    <xsl:value-of select="Videojuego[$num]/InfoGeneral/Desarrollador"/>
                </xsl:element>
                <Descripcion><xsl:value-of select="Videojuego[$num]/InfoGeneral/Descripcion"/></Descripcion>
            </InfoGeneral>
            <Plataformas>
                <xsl:for-each select="Videojuego[$num]/Plataformas/Plataforma">
                    <xsl:element name="Plataforma">
                        <xsl:attribute name="Precio">
                            <xsl:value-of select="@Precio"/>
                        </xsl:attribute>
                        <xsl:attribute name="FechaSalida">
                            <xsl:value-of select="@FechaSalida"/>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:for-each>
            </Plataformas>
            <ModosJuego>
                <xsl:for-each select="Videojuego[$num]/ModosJuego/ModoJuego">
                    <ModoJuego><xsl:value-of select="."/></ModoJuego>
                </xsl:for-each>
            </ModosJuego>
            <Imagenes>
                <xsl:for-each select="Videojuego[$num]/Imagenes/Imagen">
                    <Imagen>
                        <xsl:value-of select="."/>
                    </Imagen>
                </xsl:for-each>
            </Imagenes>
        </Videojuego>
    </xsl:template>
</xsl:stylesheet>

