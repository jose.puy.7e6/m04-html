<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="Videojuego">
        <div class="imagenes">
            <xsl:for-each select="Imagenes/Imagen">
                <xsl:element name="img">
                    <xsl:attribute name="src"><xsl:value-of select="."/></xsl:attribute>
                </xsl:element>
            </xsl:for-each>
        </div>
        <article class="article">
            <h2><xsl:value-of select="Nombre"/></h2>
            <ul>
                <li><span>Desarrollador:</span> <i><xsl:value-of select="InfoGeneral/Desarrollador"/></i></li>
                <li><span>Publisher:</span> <i><xsl:value-of select="InfoGeneral/Distribuidor"/></i></li>
                <li><span>Plataformas:</span>
                    <xsl:for-each select="Plataformas/Plataforma">
                        <xsl:value-of select="."/> 
                    </xsl:for-each>
                </li>
                <li><span>Género:</span> <xsl:value-of select="Genero"/></li>
            </ul>
            <p><xsl:value-of select="InfoGeneral/Descripcion"/></p>
        </article>
    </xsl:template>
</xsl:stylesheet>

