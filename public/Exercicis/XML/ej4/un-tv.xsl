<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <cadena>
            <nom>Un TV</nom>
            <programas>
            <xsl:apply-templates/>
            </programas>
        </cadena>
    </xsl:template>

    <xsl:template match="programacio">
        <xsl:for-each select="audiencia"> 
                <xsl:element name="programa">
                    <xsl:attribute name="hora">
                        <xsl:value-of select="hora"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="cadenes"/>
                </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="cadenes">
            <xsl:for-each select="cadena">
                <xsl:if test="@nom='Un TV'">
                    <xsl:element name="nom-programa">
                        <xsl:value-of select="."/>
                    </xsl:element>
                    <xsl:element name="audiencia">
                        <xsl:value-of select="./@percentatge"/>
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>