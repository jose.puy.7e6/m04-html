<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:variable name="muse" select="discos/group[@id='museId']/@id"/>
    <xsl:variable name="feeder" select="discos/group[@id='feederId']/@id"/>
    <xsl:template match="/">
        <lista>
            <xsl:apply-templates select="disco"/>
        </lista>
    </xsl:template>

    <xsl:template match="disco">
        <xsl:element name="disco">
            <xsl:value-of select="title"/> 
            es interpretado por:
            <p><xsl:value-of select="$muse"/></p>
            <xsl:choose>
                <xsl:when test="interpreter[@id = '$muse']">
                    <xsl:value-of select="/discos/group/@id='$muse'/name"/> 
                </xsl:when>
                <xsl:when test="interpreter[@id = '$feeder']">
                    <xsl:value-of select="/discos/group/@id='$feeder'/name"/>
                </xsl:when>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
