<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <table border="1">
            <tr style="background-color:green">
                <th>Foto</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Telefono</th>
                <th>Repetidor</th>
                <th>Nota Practica</th>
                <th>Nota Examen</th>
                <th>Nota Total</th>
            </tr>
            <xsl:for-each select="evaluacion/alumno">
            <xsl:sort select="apellidos" order="ascending"/>
                <tr>
                <xsl:choose>
                    <xsl:when test="@foto='si'">
                        <th>
                            <xsl:element name="img">
                                <xsl:attribute name="src">
                                    imagenes/<xsl:value-of select="nombre"/>.png
                                </xsl:attribute>
                                <xsl:attribute name="width">
                                    100
                                </xsl:attribute>
                                <xsl:attribute name="height">
                                    100
                                </xsl:attribute>
                            </xsl:element>
                        </th>
                    </xsl:when>
                    <xsl:otherwise>
                        <th>
                            <xsl:element name="img">
                                <xsl:attribute name="src">
                                    imagenes/icono.png
                                </xsl:attribute>
                                <xsl:attribute name="width">
                                    100
                                </xsl:attribute>
                                <xsl:attribute name="height">
                                    100
                                </xsl:attribute>
                            </xsl:element>
                        </th> 
                    </xsl:otherwise>
                </xsl:choose>
                    <th><xsl:value-of select="nombre"/></th>
                    <th><xsl:value-of select="apellidos"/></th>
                    <th><xsl:value-of select="telefono"/></th>
                    <th><xsl:value-of select="@repite"/></th>
                    <xsl:apply-templates select="notas"/>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template match="notas">
        <th><xsl:value-of select="practicas"/></th>
        <th><xsl:value-of select="examen"/></th>
        <xsl:choose>
            <xsl:when test="((practicas + examen) div 2) >= 8">
                <th style="color:blue"><xsl:value-of select="(practicas + examen) div 2"/></th>
            </xsl:when>
            <xsl:when test="((practicas + examen) div 2) >= 5">
                <th><xsl:value-of select="(practicas + examen) div 2"/></th>
            </xsl:when>
            <xsl:otherwise>
                <th style="color:red"><xsl:value-of select="(practicas + examen) div 2"/></th>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
