<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <html>
            <table border="1">
                <tr>
                    <th>Fecha</th>
                    <th>Màxima</th>
                    <th>Mínima</th>
                    <th>Predicción</th>
                </tr>
                <xsl:for-each select='/root/prediccion/dia'>
                <xsl:sort select="temperatura/maxima" order="descending"/>
                    <tr>
                        <th><xsl:value-of select="@fecha"/></th>
                        <th><xsl:value-of select="temperatura/maxima"/></th>
                        <th><xsl:value-of select="temperatura/minima"/></th>
                        <th><img src="{concat('imagenes/',estado_cielo[@periodo='00-12']/@descripcion)}.png" width="100" height="100"/></th>
                    </tr>
                </xsl:for-each>
            </table>
        </html>
    </xsl:template>
</xsl:stylesheet>
